var map = L.map('main_map').setView([3.456365, -76.468079], 15);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

$.ajax({
    type: 'GET',
    dataType: "json",
    url: "api/bicicletas",
    success: function(result) {
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, { title: bici.id }).addTo(map);
        });
    },
    error: function(request, error) {
        alert("No se pudo hacer la petición: " + error);
    }
})