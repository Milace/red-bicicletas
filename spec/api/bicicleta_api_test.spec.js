var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
const { ServiceUnavailable } = require('http-errors');

var base_url = 'http://localhost:5000/api/bicicletas';
var headers = { 'content-type': 'application/json' };

describe('Bicicleta API', () => {
    beforeAll(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        const opt = {
            keepAlive: 1,
            useUnifiedTopology: true,
            useNewUrlParser: true,
            useCreateIndex: true,
            useFindAndModify: true
        };
        mongoose.connect(mongoDB, opt);

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function() {
            console.log('Conexion a la base de datos testdb, exitosa!');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body) {
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicleta.length).toBe(0);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('STATUS 204', (done) => {
            var aBici = '{"id": 10, "color": "rojo", "modelo": "urbano", "lat": -34, "lng": -54}';
            request.post({
                headers: headers,
                url: base_url + '/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                expect(bici.color).toBe("rojo");
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-54);
                done();
            });
        })
    });
});